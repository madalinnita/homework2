const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

let products = [
    {
        id: 0,
        productName: 'Samsung Galaxy S9',
        price: 4000
    },
    {
        id: 1,
        productName: 'Iphone XR',
        price: 3999
    }
];

app.get('/get-all', (req, res) => {
    res.status(200).send(products);
})

app.post('/add', (req, res) => {
    if(req.body.productName && req.body.price){
        let product = {
            id: products.length,
            productName: req.body.productName,
            price: req.body.price
        };
        products.push(product);
        res.status(200).send(product);
    } else {
        res.status(500).send('Error!');
    }
});

app.put('/update:id', (req, res) => {
    products.findById(req.params.id).then((id) => {
        if(id) {
            id.update(req.body).then((result) => {
                res.status(201).json(result)
            }).catch((err) => {
                console.log(err)
                res.status(500).send('Error - update')
            })
        } else {
            res.status(404).send('Error - update')
        }
    }).catch((err) => {
        console.log(err)
        res.status(500).send('Error - update')
    });
});

app.delete('/delete:productName', (req, resp) => {
    products.findById(req.params.productName).then((productName) => {
        if(productName) {
            productName.destroy().then((result) => {
                resp.status(204).send()
            }).catch((err) => {
                console.log(err)
                resp.status(500).send('Error - delete')
            })
        } else {
            resp.status(404).send('Error - delete')
        }
    }).catch((err) => {
        console.log(err)
        resp.status(500).send('Error - delete')
    })
})


app.put('/put:id', (request, response) => {
    products.findById(request.params.id).then((resource) => {
        if(resource) {
            resource.update(request.body).then((result) => {
                response.status(201).json(result)
            }).catch((err) => {
                console.log(err)
                response.status(500).send('database resource error')
            })
        } else {
            response.status(404).send('resource not found')
        }
    }).catch((err) => {
        console.log(err)
        response.status(500).send('database resource error')
    })
});

app.listen(8080, () => {
    console.log('Server started on port 8080...');
});