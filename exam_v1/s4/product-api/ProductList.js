import React from 'react';

export class  ProductList extends React.Component {
    
    constructor(props){
        super(props);
        this.state = {
            source: []
        };
    }
    
    render(){
        let items = this.props.source.map((item, index) => 
            <div key={index}>{item.id}</div>
        );
        return (
            <React.Fragment>
            <div>
                <div className="items-container">
                {items}
                </div>
            </div>
            </React.Fragment>
            )    
    }
    
}